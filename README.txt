//////////////////////  README for Simulating Microneurography data  /////////////////////////

Note that the mod files containing the ion channel mechanisms must be compiled using mknrndll after migrating the files to a different machine or after making any edits. The resulting dll file must be stored in the directory in which the Python files are stored. 

There are five Python files which complete different simulation tasks:

	defineCell.py - inserts ion channel mechanisms, sets temperature and balances membrane potential
	main.py - runs simulation
	plot.py - plots latency
	saveData.py - contains functions for recording simulation data
    stimulationProtocol.py - defines the stimulation protocols

To run the simulation you have to call main.run(nseg, prot). nseg is the number of segments used, which defines the spatial resolution of the simulation. nseg=600 gives good accuracy. prot is the number of the protocol you want to use. 

implemented protocols:
	0 - 10 pulses at 2Hz
	1 - single pulse after 2 ms
	2 - 10 pulses at 4Hz, 4 pulses at 10Hz in a loop with varying pauses (10x2s, 10x1s, 10x0,2s)
	3 - High frequency protocol from Tigerholm
	4 - Low frequency protocol from Tigerholm
	5 - 10 pulses at 1Hz
	6 - 10 pulses at 4Hz, 4 pulses at 10Hz, 2second pause, 5 pulses at 4Hz
	7 - 10 pulses at 10Hz
	8 - read protocol from file
