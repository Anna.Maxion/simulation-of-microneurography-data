from neuron import h, gui
from matplotlib import pyplot as plt
import numpy as np
import csv

from defineCell import *
from stimulationProtocols import *
from saveData import *
from plot import *


def run(prot, nseg):
    # define sections
    axon = h.Section(name='axon')

    axon.diam = 1 #diameter in mu meter
    #axon.L = 100000 #length in mu meter -> 10cm (Tigerholm)
    axon.L = 5000 #Grill
    #axon.nseg = int(axon.L*6/50)
    axon.nseg = nseg

    #print(axon.psection())

    #insert channels
    insertChannels(axon)

    axon.Ra = 35.5
    axon.cm = 1
    
    #h.dt = 0.005
    #h.dt = 0.01
    
    #variable time step integration method
    cvode = h.CVode()
    cvode.active()

    #0: test protocol, 1: single pulse, 2: protocol from Barbara, 3: high frequency, 4: low frequency
    #5: test protocol, 6: test protocol
    #prot = 1
    stim, delay, vec = setStimulationProtocol(axon, prot)

    #recording
    t = recordT()
    v_beg = recordV(axon(0))
    v_beg2 = recordV(axon(0.1))
    v_mid = recordV(axon(0.5))
    v_end = recordV(axon(1))

    v = []
    spTimes = h.Vector(v) 
    apc = h.APCount(axon(1))
    apc.thresh = -10
    apc.record(spTimes)

    #simulation
    Vrest=-55
    h.finitialize(Vrest)

    tempCelsius = 37
    setTemp(axon, tempCelsius)

    h.fcurrent()

    balance(axon, Vrest)

    tstop = delay
    h.continuerun(tstop)

    #save data
    filename = 'data_Prot'+str(prot)+'_nseg'+str(nseg)+'.csv'
    with open(filename,'w') as f:
        csv.writer(f).writerows(zip(*(t, v_beg, v_beg2, v_mid, v_end)))

    #plotting
    plt.figure(figsize=(15,5))
    plt.plot(t,v_beg)
    plt.xlabel('t (ms)')
    plt.ylabel('v (mV)')
    plt.title('axon(0)')
    plt.show()

    plt.figure(figsize=(15,5))
    plt.plot(t,v_beg2)
    plt.xlabel('t (ms)')
    plt.ylabel('v (mV)')
    plt.title('axon(0.1)')
    plt.show()

    plt.figure(figsize=(15,5))
    plt.plot(t,v_mid)
    plt.xlabel('t (ms)')
    plt.ylabel('v (mV)')
    plt.title('axon(0.5)')
    plt.show()

    plt.figure(figsize=(15,5))
    plt.plot(t,v_end)
    plt.xlabel('t (ms)')
    plt.ylabel('v (mV)')
    plt.title('axon(1)')
    plt.show()

    #plot and save
    l = plotLatency(spTimes, vec)
    
    #save to file
    filename = 'APL_Prot'+str(prot)+'_nseg'+str(nseg)+'.csv'
    with open(filename,'w') as f:
        csv.writer(f).writerows(zip(*(spTimes, l)))

