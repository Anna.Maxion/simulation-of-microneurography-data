from neuron import h, gui

def recordV(section):
    v = h.Vector()
    v.record(section._ref_v)
    return v

def recordT():
    t = h.Vector()
    t.record(h._ref_t)
    return t