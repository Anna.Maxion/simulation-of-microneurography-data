from neuron import h
import numpy as np

def setStimulationProtocol(axon, prot):
    i=0
    iclamps = []
    vec = []
    delay = 0
    if prot == 0:#testprotocol
        vec = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500]
        delay = 5000
    elif prot == 1:#single pulse after 2 ms
        vec = [2]
        delay = 50
    elif prot == 2:
        vec, delay = getStimProt()
    elif prot == 3:#High frequency protocol from Tigerholm
        vec, delay = getTigerholmHighfreq()
    elif prot == 4:#Low frequency protocol from Tigerholm
        vec, delay = getTigerholmLowfreq()
    elif prot == 5:
        vec = [0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000]
        delay = 10000
    elif prot == 6:
        vec = [0, 4000, 8000, 12000, 16000, 20000, 24000, 28000, 32000, 36000, 37700, 37800, 37900, 38000, 40000, 44000, 48000, 52000, 56000]
        delay = 57000
    elif prot == 7:
        vec =[0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
        delay = 1100
    elif prot == 8:#get protocol from file
        vec, delay = getProtFromFile()
    for t in vec:
        #current clamp
        i = h.IClamp(axon(0))
        i.delay = t # ms
        i.dur = 0.2 # ms 0.5
        i.amp = 2.5 # nA 50
        #i.dur = 5 # Tigerholm
        #i.amp = 0.01 #Tigerholm
        iclamps.append(i)
    return iclamps, delay, vec

def getStimProt():
    vec=[]
    delay = 0
    RegPulse = 4000 #waittime between the regular pulses (underlying frequency)
    extraPulse=100 #waittime between the extra pulses
    for j in range(3):
        if j == 0:
            distNextReg = 2000 #distance between last of the extra pulses and next regular pulse
        elif j == 1:
            distNextReg = 1000
        elif j == 2:
            distNextReg = 200
        #repeat 10 times
        for i in range(10):
            #10 regular pulses
            vec.append(delay)
            for jj in range(9):
                delay = delay+RegPulse
                vec.append(delay)

            #4 extra pulses
            dist = RegPulse-distNextReg-3*extraPulse
            delay = delay+dist
            vec.append(delay)
            for jj in range(3):
                delay = delay+extraPulse
                vec.append(delay)

            #distance until next regular pulse
            delay = delay+distNextReg
    #print(vec)
    return vec, delay

def getTigerholmHighfreq():
    vec=[]
    delay = 10
    vec.append(delay)
    for x in range(360):#360 pulses with 2Hz frequency
        delay = delay+500
        vec.append(delay)
    for x in range(60):#60 pulses with 0.25Hz frequency
        delay = delay+4000
        vec.append(delay)
    #print(vec)
    delay = delay+100
    return vec, delay

def getTigerholmLowfreq():
    vec=[]
    delay = 10
    vec.append(delay)
    for x in range(20):#20 pulses with 1/8Hz frequency
        delay = delay+8000
        vec.append(delay)
    for x in range(20):#20 pulses with 1/4Hz frequency
        delay = delay+4000
        vec.append(delay)
    for x in range(30):#30 pulses with 1/2Hz frequency
        delay = delay+2000
        vec.append(delay)
    for x in range(20):#20 pulses with 1/4Hz frequency
        delay = delay+4000
        vec.append(delay)
    #print(vec)
    delay = delay+100
    return vec, delay
        
def getProtFromFile():    
    lines = open("timestamps_stim.txt").readlines()
    #lines = open("TestProt.txt").readlines()
    vec =[]
    for l in lines:
        l = float(l.replace("\n",""))*1000
        vec.append(l)
    delay = float(vec[len(vec)-1])+100
    return vec, delay


        
        
        
        
