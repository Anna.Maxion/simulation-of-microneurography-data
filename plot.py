from neuron import h
from matplotlib import pyplot as plt
import numpy as np
    
def plotLatency(data_aps, data_stim):
    l = []
    j=0
    i=0
    while i < len(data_stim):
        if j < len(data_aps):
            point = data_aps[j]-data_stim[i]
            #print(point)
            if point>0:
                l.append(point)
                j=j+1
                i=i+1
            else:
                j=j+1
        else:
            break
    
    plt.figure(figsize=(15,5))
    if len(data_stim)==1:
        plt.plot(1,l[0],'ro')
    else:
        plt.scatter(range(len(data_stim)),l)
    plt.xlabel('number of input')
    plt.ylabel('latency (ms)')
    plt.title('Latency')
    plt.show()
    return l

    
    
        
